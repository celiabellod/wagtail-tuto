.DEFAULT_GOAL := help
.PHONY: help
help:
	@echo "\033[33mUsage:\033[0m\n  make [target] [arg=\"val\"...]\n\n\033[33mTargets:\033[0m"
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' Makefile| sort | awk 'BEGIN {FS = ":.*?## "}; {printf "  \033[32m%-15s\033[0m %s\n", $$1, $$2}'

.PHONY: bash
bash: ## Get a shell into app container
	docker-compose exec app bash

.PHONY: logs
logs: ## Get a shell into app container
	docker-compose logs -f app

.PHONY: install
install:  ## Install project
	docker-compose up --build -d

.PHONY: up
up: ## Start containers
	docker-compose up -d

.PHONY: stop
stop: ## Stop containers
	docker-compose exec app pip freeze > ./app/requirements.txt
	-docker-compose stop

.PHONY: restart
restart: ## Restart containers
	docker-compose restart

.PHONY: db-dump
db-dump: ## backup db 
	docker-compose exec postgres /usr/bin/pg_dump -U root project > ./app/sql-database/backup.sql

.PHONY: db-restore
db-restore: ## Restore db
	docker-compose exec -T postgres /bin/bash -c "PGPASSWORD=root psql --username root project" < project/app/sql-database/backup.sql

.PHONY: db-init
db-init:  ## Install project
	docker-compose exec app python dbInit.py

.PHONY: hash-password
hash-password:  ## Install project
	docker-compose exec app python hashPasswordCommande.py

.PHONY: fixtures
fixtures:  ## make fixtures
	docker-compose exec app python -m appli.fixtures.main

.PHONY: dependances
dependances:  ## make fixtures
	docker-compose exec app pip freeze > ./app/requirements.txt

.PHONY: migrations
migrations:  ## make migrations
	docker-compose exec app python manage.py makemigrations

.PHONY: migrate
migrate:  ## push migrations
	docker-compose exec app python manage.py migrate

.PHONY: cc
cc:  ## clear cache
	find ./app | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs sudo rm -rf

.PHONY: tests
tests:  ## Install project
	docker-compose exec app python manage.py test polls